//
// Created by Kiouche on 1/20/2020.
//

#ifndef P_K_COMPRESSION_IO_H
#define P_K_COMPRESSION_IO_H
#include "graph.h"
#include <boost/program_options.hpp>

namespace po = boost::program_options;


namespace  std {
    tuple<graph,unordered_map<edge , double>>  read_graph_from_file(string filename,bool directed);
    void graph_to_file (po::variables_map &var,double runtime, double c_rate,vector<edge> &edges);

}

#endif //P_K_COMPRESSION_IO_H
