//
// Created by Kiouche on 1/20/2020.
//

#ifndef P_K_COMPRESSION_GRAPH_H
#define P_K_COMPRESSION_GRAPH_H
#include <unordered_map>
#include <unordered_set>
#include <vector>





namespace std{

    typedef  unordered_map<uint32_t,unordered_map<int,unordered_set<uint32_t >>> node_neighbors; /// key 1 = node_id , key 2 = depth , value = nodes

    typedef unordered_map<uint32_t , unordered_set<uint32_t >>  graph; /// adjacency matrix , key = node_id

    typedef pair<uint32_t , uint32_t> edge;

    vector<edge> get_edges(graph &g,bool directed);


    bool is_it_undirected(graph &g);


    unordered_set<uint32_t > intersection(unordered_set<uint32_t> &s1,unordered_set<uint32_t> &s2);


}



#endif //P_K_COMPRESSION_GRAPH_H
