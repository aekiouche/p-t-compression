//
// Created by Kiouche on 1/20/2020.
//

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <vector>
#include "graph.h"
#include "io.h"
#include "hash.h"

namespace  std {

    tuple<graph,unordered_map<edge , double>>  read_graph_from_file(string filename,bool directed) {

        ifstream f(filename);
        string line;
        graph g;
        unordered_map<edge , double>   edges_scores;
        int loops = 0;

        if (f.is_open()) {
            while (getline(f, line)) {
                uint32_t src_id, dst_id;
                edge e,e_r;
                double score;
                stringstream ss;
                ss.str(line);
                ss >> src_id;
                ss >> dst_id;
                ss >> score;

                if (src_id!=dst_id) {
                    e.first = src_id ; e.second = dst_id;
                    e_r.first = dst_id; e.second = src_id;
                    g[src_id].insert(dst_id);
                    if(!directed) g[dst_id].insert(src_id);
                    edges_scores[make_pair(src_id,dst_id)] = score ;
                   // if(!directed) edges_scores[e_r] = score;
                }
                else loops++;
            }
        } else  {
            cout << "Unable to open " << filename << " ! \n";
            exit(-1);
        }
        cout << "number of nodes = " << g.size() << endl;
        cout << "loops = " << loops << endl;
        return {g,edges_scores};
    }

    void graph_to_file (po::variables_map &var,double runtime, double c_rate,vector<edge> &edges){

        ofstream file;
        file.open(var["output_file"].as<string>());
        file << "Original graph" << "\t"<< var["input"].as<string>() <<endl;
        file << "Directed" << "\t" << to_string(var["directed"].as<bool>()) << endl;
        file << "Depth" << "\t" << to_string(var["depth"].as<int>()) << endl;
        vector<double> p_values = var["proportions"].as<vector<double>>();
        int depth = var["depth"].as<int>();
        for (int i = 0;i<=depth;i++){
            file << "k " + to_string(i) << "\t" << to_string(p_values.at(i)) << endl;
        }
        file << "execution time " << "\t" << to_string(runtime) << endl;
        file << "compression rate " << "\t" << to_string(c_rate) << endl;

        file << endl;

        for (auto e : edges){
            file << e.first <<"\t"<< e.second << endl;
        }
        file.close();
    }

}